var Auth = function () {
  var t = this;
  t.name = document.getElementById("name");
  t.password = document.getElementById("password");
  t.confirmPassword = document.getElementById("confirm_password");
  t.form = document.getElementById("auth_form");

  t.inputs = [t.name, t.password, t.confirmPassword];

  t.confirmPassword.onpaste = function (e) { e.preventDefault(); }

  for (var i = 0; i < t.inputs.length; i++) {
    t.inputs[i].addEventListener("keyup", function () { t.verifyErrors(); })
  }

  t.errors = {
    NMR: false, // name required
    MPW: false, // password and confirm password matching
    PWL: false, // password length
  }

  t.resetErrors = function () {
    for (var code in t.errors) {
      if (t.errors.hasOwnProperty(code)) { t.errors[code] = false; }
    }
  }

  t.updateErrors = function () {
    if (t.name.value.length < 8) t.errors.NMR = true;
    if (t.password.value != t.confirmPassword.value) t.errors.MPW = true;
    if (t.password.value.length < 8) t.errors.PWL = true;
    t.updateErrorsView();
  }

  t.updateErrorsView = function () {
    for (var code in t.errors) {
      if (t.errors.hasOwnProperty(code)) {
        var state = t.errors[code];
        var el = document.getElementById("error-" + code);
        if (el) el.setAttribute("state", state ? "1" : "0");
      }
    }
  }

  t.verifyErrors = function () {
    t.resetErrors();
    t.updateErrors();
  }

  t.formInvalid = function () {
    for (var code in t.errors) {
      if (t.errors.hasOwnProperty(code) && t.errors[code] == true) { return 1 }
    }
  }

  t.form.onsubmit = function (e) {
    if (t.formInvalid()) return e.preventDefault();
    console.log("form valid");
  }
}

var a = new Auth()

