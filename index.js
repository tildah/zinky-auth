const Zinko = require('zinko');
const randomstring = require("randomstring");
const _ = require('lodash');
const ObjectId = require('mongodb').ObjectID;
const crypto = require('crypto');
const validator = require("email-validator");


class Auth extends Zinko {

  get roles() { return ['admin'] }

  get adminRole() { return this.roles[0] }

  render(viewName, req, res) {
    req.flash = req.cookies.flash;
    res.clearOneCookie('flash');
    req.type = viewName;
    let v = this.v(viewName);
    v.extends(this.v("L_auth"));
    return v.render(req);
  }

  GET_root(req, res) {
    return this.render("login", req, res)
  }

  GET_register(req, res) {
    return this.render("register", req, res)
  }

  harasHudood(req) {
    if (_.isEmpty(req.user) && req.module.authRequired) res.redirect('/auth/');
  }

  isAdmin(req) {
    return req.user.role == this.adminRole;
  }

  async POST_register(req, res) {
    try {
      if (!req.body.name)
        throw { code: 400, msg: "missing name" };
      if (req.body.name.length < 8)
        throw { code: 400, msg: "short name" };
      if (!req.body.email)
        throw { code: 400, msg: "missing email" };
      if (!req.body.password)
        throw { code: 400, msg: "missing password" };
      if (!validator.validate(req.body.email))
        throw { code: 400, msg: "email not valid" };
      if (req.body.password.length < 8)
        throw { code: 400, msg: "short password" };
      if (req.body.password != req.body.confirm_password)
        throw { code: 400, msg: "password doesn't match" };

      const filterExisting = { email: req.body.email };
      const existingUser = await req.app.db.collection("users").findOne(filterExisting);
      if (existingUser) throw { code: 400, msg: "already used email" };

      const hash = crypto.createHash('sha256');
      var newUser = _.pick(req.body, ["name", "email", "password"]);
      newUser.password = hash.digest(newUser.password);
      newUser.role = "normal";
      await req.app.db.collection("users").insertOne(newUser);

      req.hashedPwd = newUser.password;
      this.POST_login(req, res);

    } catch (e) {
      console.error(e);
      if (e.code != "400") e = { code: "500", msg: "500" };
      res.cookie('flash', e.msg, { maxAge: 3600000, httpOnly: true });
      res.redirect('/auth/');
    }

  }

  async POST_login(req, res) {
    try {
      var session = await this.login(req);
      res.cookie('s_id', session._id, req.app.oneYearCookie);
      res.cookie('s_key', session.key, req.app.oneYearCookie);
      res.redirect('/');
    } catch (e) {
      console.error(e);
      if (e != "401") e = "500";
      res.cookie('flash', e, { maxAge: 3600000, httpOnly: true });
      res.redirect('/auth/');
    }
  }

  async login(req) {
    if (!req.body.email || !req.body.password) throw '401';
    const hash = crypto.createHash('sha256');
    var filter = {
      email: req.body.email,
      password: hash.digest(req.body.password),
      trash: { $ne: true }
    }
    var user = await req.app.db.collection("users").findOne(filter);
    if (!user) throw "401";
    var session = {
      user_id: user._id,
      key: randomstring.generate(30),
      createdAt: new Date(),
      trash: false,
      deletedAt: null
    };
    var s_r = await req.app.db.collection("sessions").insertOne(session);
    return s_r.ops[0];
  }

  GET_logout(req, res) {
    res.clearOneCookie('s_id', true);
    res.clearOneCookie('s_key', true);
    res.redirect('/');
  }

  async loadSession(req) {
    req.user = {};
    if (req.cookies.s_id && req.cookies.s_key) {
      var sessionFilter = {
        _id: new ObjectId(req.cookies.s_id),
        key: req.cookies.s_key,
        trash: false
      };
      var s = await req.app.db.collection("sessions").findOne(sessionFilter);
      if (s) {
        var uFilter = { _id: new ObjectId(s.user_id), pause: { $ne: true } };
        var u = await req.app.db.collection("users").findOne(uFilter);
        req.user = u;
      }
    }
  }

  async deleteSessions(req) {
    var filter = { user_id: new ObjectId(req.user_id) };
    var r = await req.app.db.collection("sessions").deleteMany(filter);
  }

}

module.exports = Auth;